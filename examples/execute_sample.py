"""
This script executes a config .json
"""
from pathlib import Path
import sys

from declair.execution import execute_file
from declair.env import Environment

if __name__ == '__main__':
    configs_directory = Path(__file__).parent.joinpath('sample_configs')

    if len(sys.argv) < 2:
        print("""Usage:
        python -m examples.execute_sample <config_name> [env_config_file]

    where 
      - <config_name> is one of the json files from examples/sample_configs.
      - [env_config_file] is an optional environment config, which defines 
        where run results are stored. Default value is `declair_env.yaml`

    Possible <config_name> values:""")
        for file_ in configs_directory.glob("*"):
            if 'declair_env' not in file_.name and not file_.name.startswith('.'):
                print("\t{}".format(file_.name))
        print("""\n    Possible [env_config_file] values:""")
        for file_ in configs_directory.glob("*"):
            if 'declair_env' in file_.name and not file_.name.startswith('.'):
                print("\t{}".format(file_.name))
        exit(0)

    config_name = sys.argv[1]
    if len(sys.argv) == 3:
        env_name = sys.argv[2]
    else:
        env_name = 'declair_env.yaml'
    full_path = configs_directory.joinpath(config_name).absolute()
    env_path = configs_directory.joinpath(env_name).absolute()
    env = Environment.from_file(env_path)
    execute_file(full_path, env=env)
