"""
tests.sample_project
====================
Subpackage containing modules of a sample declair project.

This project compares performance of various models on UCI ML hand-written
digits datasets.

https://archive.ics.uci.edu/ml/datasets/Optical+Recognition+of+Handwritten+Digits
"""
