"""
This test file only makes sure the samples don't crash. It runs them, but it
doesn't save any outputs.
"""
from pathlib import Path
from shutil import rmtree
from tempfile import mkdtemp

from declair.execution import execute_file
from declair.env import Environment

def _filter_example_experiment(path):
    name = path.name
    if name.endswith('.json'):
        return True
    elif name.endswith('.yaml') and 'declair_env' not in name:
        return True
    else:
        return False

def pytest_generate_tests(metafunc):
    configs_directory = Path(__file__).parent.joinpath('sample_configs')
    metafunc.parametrize("config_path", filter(
        _filter_example_experiment, configs_directory.glob("*")))

def test_run_experiment(config_path):
    tmpdir = mkdtemp()
    env = Environment({'observers': {'file': {'path': tmpdir}}})
    # one of the runs makes use of the environment variable defined below
    if "run_with_environment_elements" in str(config_path.absolute):
        env.update_from_dict({
            'constraints': {'epoch_limit': 3}}, 'test')
    execute_file(config_path, env=env)
    rmtree(tmpdir)
