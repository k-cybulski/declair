+++
title = "Declair: a hyperparameter search configuration framework"

insert_anchor_links = "left"

[extra]
author = "Krzysztof Cybulski<sup>12✉</sup>, Tim De Jong<sup>2</sup>, Marco Puts<sup>2</sup>"
institutions = "<sup>1</sup>Maastricht University</br><sup>2</sup>Statistics Netherlands</br><sup>✉</sup>Contact at <a href=\"mailto:declair@kcyb.eu\">declair@kcyb.eu</a>"
+++

Declair[^declair] is a framework for defining, performing, and tracking hyperparameter search experiments in complex spaces with a focus on reproducibility. It provides a YAML/JSON configuration based language for describing parameter search spaces which can include Python types and outputs of function calls with an arbitrary level of nesting. Furthermore, it includes various ergonomic features like configuration inheritance and environment variable handling. By leveraging Sacred[^sacred] for results tracking and Hyperopt[^hyperopt] for performing optimization, it combines the best of both worlds for a reproducible hyperparameter search workflow.

## Introduction

In machine learning, especially deep learning, models frequently require many hyperparameters which affect each other in a complex, nonlinear fashion. When evaluating a new architecture and comparing it to others, it is often not clear whether one outperforms another due to its inherent superiority, or simply due to a subpar combination of hyperparameters for the other. By evaluating all candidate models with hyperparameters found by solid optimization algorithms, we can be more confident in our results. Many good tools already exist for performing hyperparameter search, like Hyperopt[^hyperopt], Optuna[^optuna], or Ray.tune[^ray_tune]. However, it is not clear how these can be combined with complex results tracking code while making sure experiments are easy to reproduce by other researchers.

Building upon previous work, we propose Declair, an open framework for defining, performing and tracking hyperparameter search experiments in complex spaces with a focus on reproducibility. It combines Sacred[^sacred], an open experiments tracking library with Hyperopt[^hyperopt], an optimization framework, and expands their capabilities with features for easy experiment reproducibility between machines. Altogether, it forms a solid basis for a pipeline in machine learning architecture research.

By relying on Sacred for results tracking, Declair allows for any machine learning framework to be used underneath for experiments. It was mainly tested with PyTorch, but it is now also being used with Keras/Tensorflow code and would be easily adapted to scikit-learn as well.

### Comparison to other tools

Labwatch[^labwatch] is the tool most similar to Declair, in that they both perform hyperparameter optimization and wrap around Sacred to store run results. The key difference is that Declair is focused on complex search spaces&mdash;with arbitrary nesting and dynamically instantiated Python objects&mdash;at a cost of simplicity and tight coupling with Sacred. Declair relies on YAML configurations to define experiments, while Labwatch simply uses Python functions and ties closely with idiomatic Sacred. By using YAML, Declair makes it easy to modularize experiments into separate files. This modularization allows for configuration inheritance, which in turn can greatly decrease amount of boilerplate code necessary to define many similar-yet-different experiments, such as when various specific parameters are fixed.

Guild AI[^guild_ai] is an open source experiment tracking and hyperparameter search system that, similar to Declair, does not require any hosted services. It is based on a collection of command line tools that allow for managing experiments with little to no changes to underlying code. Guild collects results of experiment scripts by tracking their standard output for metric updates, or by TensorBoard summaries. File artifacts, like generated plots or saved models, are simply gathered from what the script is programmed to store in the current working directory. In contrast to Declair/Sacred, Guild only supports storing results on the filesystem with no support of databases like MongoDB. Furthermore, Guild search spaces are limited to numerics, booleans, and strings, with no conditional branches or nesting&mdash;as opposed to Declair's arbitrary nestedness and Python objects.

Common solutions for hyperparameter optimization involve the use of libraries like Hyperopt[^hyperopt], Optuna[^optuna], or Ray Tune[^ray_tune], coupled with a results tracking service like MLflow[^mlflow] or its proprietary alternatives[^proprietary]. These workflows support similar or even more complex search spaces than Declair (with a Hyperopt backend); specifically, Optuna supports conditional loops which are not feasible to define in YAML without adding great complexity. The distinguishing features of Declair compared to those setups are:
 - Sacred on the backend, which provides great reproducibility features like automated source code saving and random seed tracking, as well as its ecosystem of open source dashboards[^sacred_dashboards].
 - Experiment definition inheritance, allowing for many similar-yet-different experiments to be defined without repeating boilerplate. This can be useful when, for example, the same search needs to be performed on multiple datasets separately, or when some experiments consider fixed values of specific parameters.
 - Focus on ease of code re-use between machines without extra infrastructure like hosted services. It is sufficient to modify the environment configuration file `declair_env.yaml` with e.g. local paths of datasets to be able to run and analyse Declair experiments.
 - Flexibility in analysis. Declair comes with helper functions for filtering and processing stored results into Pandas DataFrames[^pandas_dataframe] without making assumptions about what the runs output.

## Runs and search definitions

Declair experiments are defined using a YAML/JSON configuration based language. They're divided into individual runs, where an _execute function_ is executed with some parameters, and into searches consisting of many such runs. Hyperparameter searches allow for sampling run definitions from a wide space that may include arbitrarily nested conditional branches, numeric variables or&mdash;with the use of specially parsed configuration entries&mdash;even Python types or instantiated objects. Thanks to this, it is possible to write experiment functions which simply take objects/types from the parameter dictionary as a given, without need for further parsing.

A commented example of a Declair search space definition is provided below to illustrate how disjoint branches, numeric variables, and Python objects are defined in YAML. In this scenario, we wish to investigate how various scikit-learn models perform on the `sklearn.datasets.load_digits` dataset. We assume there is a function `research_project.execute.execute_experiment` which implements the procedure and takes in a scikit-learn model object and dataset as parameters.

<div class='filename'>
  <div>search_digits.yaml</div>
</div>

```yml
# type could be "run" if it's just an individual run
type: "search"
# mode could be "grid" in case we didn't want to use Hyperopt's optimizer
mode: "hyperopt"
# Execution function called to perform the experiment
execute_function: {__type__: "research_project.execute.execute_experiment"}

# Configuration of Hyperopt for hyperparameter search
search_params:
    # Hyperopt fmin kwargs
    fmin_kwargs:
        max_evals: 50
        algo: {__type__: "hyperopt.tpe.suggest"}
    # What metric to optimize?
    optimize:
        type: "max"
        target: "cv_score"

# arguments of the execute function will be sampled from the parameter search
# space defined below
params:
    # Every list in a search space definition defines a disjoint choice. Below,
    # the model is chosen from two options with multiple smaller sub-choices
    model:
        # __call__ entries are dynamically loaded and called before being
        # passed on to the execute function
        - __call__: "sklearn.svm.SVC"
          kwargs:
              kernel: ['rbf', 'linear']
        - __call__: "sklearn.ensemble.RandomForestClassifier"
          kwargs:
              # n_estimators below is sampled from a numeric distribution with Hyperopt
              n_estimators:
                  __hp__: quniform
                  args: [1, 10, 1]
    dataset:
        __call__: "sklearn.datasets.load_digits"
```

## Search algorithms

Since Declair relies on Hyperopt[^hyperopt] underneath, it supports all optimization algorithms that Hyperopt supports. Beyond that, it also supports simple grid search.

## Experiment definition inheritance

Since it is common to perform experiments that are similar, yet with only a few parameters changed, Declair provides definition inheritance. That is, it is possible to define experiments that inherit all parameters except for a chosen few which are overwritten.

A simple example that extends the `search_digits.yaml` experiment above is given below. In this case, the dataset is instead `sklearn.datasets.load_wine`, but the rest of the hyperparameter search space remains the same.

<div class='filename'>
  <div>search_wine.yaml</div>
</div>

```yml
type: "extend"
# Define which file to extend from. The path is relative
# (e.g. can include double dots "../" to go up a directory)
extend_from: "search_digits.yaml"

# define only the bits that are different, everything else stays the same
params:
    dataset:
        __call__: "sklearn.datasets.load_wine"
```

## Environment and result storage configuration

To make experiments easily reproducible between machines, Declair supports inserting variables from local environment configuration files, named `declair_env.yaml`, into experiment definitions; these could be, for example, local dataset paths. Furthermore, these files are also the place to specify where results are stored. Declair, using Sacred, can store outputs of runs either locally on the filesystem or online on a MongoDB database. Support could be extended to other backends, i.e. [Sacred observers](https://sacred.readthedocs.io/en/stable/observers.html), without much effort.

An example of such a configuration file is given below.

<div class='filename'>
  <div>declair_env.yaml</div>
</div>

```yml
# which Sacred observers to use for storing results
observers:
    file:
        path: "out/test_run"
    mongo:
        url: "mongodb://mongo_user:mongo_password@localhost:27017"

# some local environment configuration that will be used by runs
data:
    cat_dog_pictures: "some/local/path"
    plants: "another/local/path"
```

Information from `declair_env.yaml` can be used within an experiment definition YAML with an entry of the form `{__env__: <path to info>}`; for example, to use the path for `cat_dog_pictures` in an experiment, the entry is `{__env__: ['data', 'cat_dog_pictures']}`.

## Execution

In order to store results like artifacts or metric logs, the execution function must take in a `_run` argument, which will be a [Sacred Run object](https://sacred.readthedocs.io/en/stable/apidoc.html#the-run-object). In line with the example search spaces `search_digits.yaml` and `search_wine.yaml` above, our experiment execution function could be implemented as:

<div class='filename'>
  <div>research_project/execute.py</div>
</div>

```py
import numpy as np
from matplotlib import pyplot as plt
from sklearn.model_selection import cross_val_score, train_test_split
from sklearn.metrics import plot_confusion_matrix

from declair.results import add_artifact_fig

def execute_experiment(params, _run):
    model = params['model'] # We assume this to be an sklearn estimator
    dataset = params['dataset'] # We assume this to be an sklearn dataset

    x = dataset['data']
    y = dataset['target']

    # First, to examine if this model performs well, we will compute its cross validated score.
    cv_score = np.mean(cross_val_score(model, x, y, cv=5))

    # Then, we would like to see which classes are being confused with which.
    # For that purpose, we will make a confusion matrix plot
    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.33, random_state=42
    )
    model.fit(x_train, y_train)

    # Create a figure and plot the matrix
    fig, ax = plt.subplots()
    plot_confusion_matrix(model, x_test, y_test, ax=ax, xticks_rotation='vertical')
    fig.set_size_inches(10, 10)

    # Save the figure as an artifact
    add_artifact_fig(_run, fig, "confusion_matrix.png")

    return {
        'cv_score': cv_score,
    }
```

To actually execute one of the experiments, say `search_digits.yaml`, we can simply run the helper script `declair-execute` that comes with Declair. It will start the hyperparameter optimization loop:
```bash
declair-execute search_digits.yaml
```

Every run outputs a Declair-specific artifact `declair_format_config.json` which is a self-contained experiment definition that may be executed with `declair-execute` on its own to repeat the run.


## Analysing results

Thanks to relying on Sacred for results storage, results of Declair experiments can be viewed in a number of frontends that support Sacred[^sacred_dashboards]. To allow for more complex analysis programmatically, Declair provides functions for parsing results and for transforming them into a Pandas DataFrame[^pandas_dataframe].

The code below illustrates how quickly it's possible to turn Declair search outputs into a table of data, as shown on sample outputs after execution of `search_digits.yaml` from examples above. The formatting is from an IPython REPL.

```py
In [1]: from declair.analysis import searches_from_dir
   ...: 
   ...: # find all searches in the default output storage directory defined in
   ...: # declair_env.yaml
   ...: searches = searches_from_dir()
   ...: 
   ...: # convenience property that returns the most recent in a list of searches/runs
   ...: search = searches.latest
   ...: 
   ...: # Make a dataframe with relevant columns
   ...: df = search.to_df(
   ...:     lambda run: {
   ...:         'cv_score': run.sacred_run.result['cv_score'],
   ...:         'model_type': run.definition.params.model.__call__
   ...:     })
   ...: 

In [2]: df
Out[2]: 
    cv_score                               model_type
id                                                   
1   0.839215  sklearn.ensemble.RandomForestClassifier
2   0.898729  sklearn.ensemble.RandomForestClassifier
3   0.963284                          sklearn.svm.SVC
4   0.963284                          sklearn.svm.SVC
5   0.904867  sklearn.ensemble.RandomForestClassifier
6   0.817507  sklearn.ensemble.RandomForestClassifier
7   0.963284                          sklearn.svm.SVC
8   0.963284                          sklearn.svm.SVC
9   0.963284                          sklearn.svm.SVC
10  0.963284                          sklearn.svm.SVC
[...]
```

Declair comes with multiple convenience functions for filtering run outputs. For instance if `search_digits.yaml` was started many times, but stopped prematurely for most of them, it is easy to find the search with the most completed runs:

```py
# searches_from_dir returns a FunctionalList which is a child class of list
# that includes methods like `filter`, `max`, or `sorted` for easy method call
# chaining
searches = searches_from_dir()

search_to_analyse = searches.filter(
    lambda search: search.name == 'search_digits.yaml'
).max(
    lambda search: search.count_completed
)
# searches_to_analyse is now the search with name 'search_digits.yaml' with the
# most completed runs
```


## Practical usecases

We have successfully used Declair to evaluate architectures in a computer vision project to detect solar panels from aerial photographs. Beyond a simple scenario of just training models on a single dataset and evaluating which performs best, we also used it for experiments on domain adaptation where the models were evaluated on multiple tasks on multiple datasets. We found that Declair made it easy to provide complex parameter combinations to our experiment functions, and that Sacred allowed for clean results tracking even in complicated experiment pipelines.

## References

[^declair]: [Declair repository](https://gitlab.com/k-cybulski/declair) and [its documentation](https://k-cybulski.gitlab.io/declair/).

[^sacred]: K. Greff, A. Klein, M. Chovanec, F. Hutter, and J. Schmidhuber, ‘The Sacred Infrastructure for Computational Research’, in Proceedings of the 15th Python in Science Conference (SciPy 2017), Austin, Texas, 2017, pp. 49–56. [Sacred repository](https://github.com/IDSIA/sacred/) and its [documentation](https://sacred.readthedocs.io/en/stable/).

[^hyperopt]: Bergstra, J., Yamins, D., Cox, D. D. (2013) Making a Science of Model Search: Hyperparameter Optimization in Hundreds of Dimensions for Vision Architectures. To appear in Proc. of the 30th International Conference on Machine Learning (ICML 2013). [Hyperopt repository](https://github.com/hyperopt/hyperopt/) and its [documentation](http://hyperopt.github.io/hyperopt/). For list of optimization algorithms available, see [here](https://github.com/hyperopt/hyperopt#algorithms)

[^optuna]: Takuya Akiba, Shotaro Sano, Toshihiko Yanase, Takeru Ohta, and Masanori Koyama. 2019. Optuna: A Next-generation Hyperparameter Optimization Framework. In KDD (arXiv). [Optuna repository](https://github.com/optuna/optuna) and its [documentation](https://optuna.readthedocs.io/en/stable/).

[^ray_tune]: [Ray repository](https://github.com/ray-project/ray) including Tune and its [documentation](https://docs.ray.io/en/master/tune/index.html).

[^labwatch]: [Labwatch repo](https://github.com/automl/labwatch) and [its documentation](https://automl.github.io/labwatch/).

[^guild_ai]: [Guild AI website](https://guild.ai/).

[^mlflow]: [MLflow website](https://mlflow.org/), [its repository](https://github.com/mlflow/mlflow/) and [documentation](https://mlflow.org/docs/latest/index.html).

[^proprietary]: Proprietary tools for results tracking include [Neptune](https://neptune.ai/), [Weights & Biases](https://wandb.ai/), or [Comet](https://www.comet.ml/).

[^sacred_dashboards]: A brief list of frontends which support Sacred is [given in the Sacred repository](https://github.com/IDSIA/sacred/#frontends).

[^pandas_dataframe]: Reback, J., McKinney, W., Jbrockmendel, Bossche, J. V. D., Augspurger, T., Cloud, P., Gfyoung, Sinhrks, Klein, A., Roeschke, M., Hawkins, S., Tratner, J., She, C., Ayd, W., Terji Petersen, Garcia, M., Schendel, J., Hayden, A., MomIsBestFriend, … Mortada Mehyar. (2020). pandas-dev/pandas: Pandas 1.0.3 (v1.0.3) [Computer software]. Zenodo. https://doi.org/10.5281/ZENODO.3715232 . [Pandas](https://pandas.pydata.org/docs/index.html) is a library for tabular data analysis, with the [DataFrame](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.html) being a table.
