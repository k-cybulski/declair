import pytest

from declair.execution import execute_file

def test_run_example():
    try:
        import torch
    except:
        pytest.skip("torch is not installed")
    execute_file('tests/sample/frameworks/ignite/run.yaml')
