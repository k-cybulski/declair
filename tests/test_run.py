import pytest
import yaml

from declair.env import Environment
from declair.exception import InvalidConfigException
from declair.serialization import serialize_type_dict
from declair.serialization import string_to_type
from declair.run.type import insert_type
from declair.run.variables import insert_var, insert_var_instance
from declair.run.execute import _config_to_execute

# Placeholder classes for testing dynamic imports
from .sample import Adam, RMSprop, Adagrad
from .sample.placeholders import func1, vgg16, alexnet, NLLLoss, MNIST

@pytest.fixture
def type_serialization_test_dict():
    # Import arbitrary objects from a bunch of libraries
    # but make it seem like it's a fancy deep learning experiment
    # for these tests, it makes no difference
    in_dict = {
        'model': (vgg16, alexnet),
        'criterion': (
            ({'criterion': (NLLLoss,),
                'weights': ((1, 2.4),)
            })
        ),
        'optimizer': (
            {
                'optimizer': (Adagrad),
                'lr': (4e-05, 4e-06, 4e-07)
            },
            {
                'optimizer': (RMSprop,),
                'lr': (1e-2, 1e-1),
                'momentum': (0, 1e-1)
            }
        ),
        'dataset': MNIST
    }
    return in_dict

def test_recursive_type_insertion_as_inverse_of_serialize_type_dict(type_serialization_test_dict):
    dict_ = yaml.safe_load(serialize_type_dict(type_serialization_test_dict))
    dict_ = insert_type(dict_)
    assert dict_ == type_serialization_test_dict

def test_variables_and_instance_variables():
    in_config = yaml.safe_load("""
params:
    dataset1:
        batch_size:
            __var__: batch_size
    dataset2:
        batch_size:
            __var__: batch_size
variables:
    batch_size: 4
""")
    inserted = insert_var(in_config, in_config['variables'])
    assert inserted['params']['dataset1']['batch_size'] == in_config['variables']['batch_size']
    assert inserted['params']['dataset1']['batch_size'] == inserted['params']['dataset2']['batch_size']

    in_config = yaml.safe_load("""
params:
    model:
        __var__: model
variables:
    batch_size: 3
""")
    try:
        insert_var(in_config, in_config['variables'])
        assert False
    except InvalidConfigException:
        assert True

    in_config = yaml.safe_load("""
params:
    model1:
        __var__: model
    model2:
        __var_instance__: model
    model3:
        __var_instance__: model
variables:
    model: {__call__: yaml.YAMLObject}
""")
    fully_loaded = _config_to_execute(in_config, Environment())
    assert isinstance(fully_loaded['params']['model1'],
                      string_to_type('yaml.YAMLObject'))
    assert fully_loaded['params']['model2'] == fully_loaded['params']['model3']
    assert fully_loaded['params']['model1'] != fully_loaded['params']['model2']

    in_config = yaml.safe_load("""
params:
    model:
        __var__: model
variables:
    model:
        architecture: vgg16
        activation: sigmoid
""")
    fully_loaded = _config_to_execute(in_config, Environment())
    assert fully_loaded['params'] == {
        'model': {
            'architecture': 'vgg16',
            'activation': 'sigmoid'
            }
    }
