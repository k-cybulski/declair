"""
Tests for declair.calling module.
"""
from math import sqrt

from declair.run.call import _call_dict, insert_call_results

def add(x, y):
    return x + y

def test_call_dict():
    cases = [
        ({'__call__': 'math.sqrt',
         'args': [9]}, 3),
        ({'__call__': 'tests.test_calling.add',
          'kwargs': {'x': 9, 'y': 3}}, 12),
        ({'__call__': sqrt,
          'args': [9]}, 3),
        ({'__call__': add,
          'kwargs': {'x': 9, 'y': 3}}, 12)]
    for dict_, out in cases:
        assert _call_dict(dict_) == out

def test_insert_call_results():
    cases = [
        ({
            "__call__": add,
            "args": [1, 2]
        }, 3),
        ({
            "trees": {
                "__call__": sqrt,
                "args": [9]
            },
            "cats": (
                5,
                {
                    "__call__": add,
                    "args": [1, 2]
                })
        }, {"trees": 3.0, "cats": (5, 3)}),
        ({'__call__': add,
          'kwargs': {
            'x': {'__call__': sqrt, 'args': [9]},
            'y': 1}}, 4.0),
        ({'__call__': add,
          'args': [
            {'__call__': sqrt, 'args': [9]},
            1]}, 4.0)]
    for dict_, out in cases:
        assert insert_call_results(dict_) == out
