"""
These classes/functions don't do anything, they're just placeholders for
testing dynamic imports.
"""

def func1():
    pass

def vgg16():
    pass

def alexnet():
    pass

class NLLLoss:
    pass

class MNIST:
    pass
