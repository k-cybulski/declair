import pytest
import json

from declair.serialization import(type_to_string,
                                  string_to_type,
                                  serialize_type_dict)
from declair.run.type import insert_type

# Placeholder classes for testing dynamic imports
from .sample import Adam, RMSprop, Adagrad
from .sample.placeholders import func1, vgg16, alexnet, NLLLoss, MNIST

def test_type_to_string():
    assert type_to_string(func1) == 'tests.sample.placeholders.func1'
    assert type_to_string(Adam) == 'tests.sample.nested_subpackage.some_module.Adam'

def test_string_to_type():
    assert string_to_type('tests.sample.placeholders.func1') == func1
    assert string_to_type('tests.sample.nested_subpackage.some_module.Adam') == Adam

@pytest.fixture
def type_serialization_test_dict():
    in_dict = {
        'model': [vgg16, alexnet],
        'criterion': [
            [{'criterion': [NLLLoss],
                'weights': [[1, 2.4]]
            }]
        ],
        'optimizer': [
            {
                'optimizer': [Adagrad],
                'lr': [4e-05, 4e-06, 4e-07]
            },
            {
                'optimizer': [RMSprop],
                'lr': [1e-2, 1e-1],
                'momentum': [0, 1e-1]
            }
        ],
        'dataset': MNIST
    }
    return in_dict

def test_serialize_type_dict(type_serialization_test_dict):
    out_string = '{"criterion": [[{"criterion": [{"__type__": "tests.sample.placeholders.NLLLoss"}], "weights": [[1, 2.4]]}]], "dataset": {"__type__": "tests.sample.placeholders.MNIST"}, "model": [{"__type__": "tests.sample.placeholders.vgg16"}, {"__type__": "tests.sample.placeholders.alexnet"}], "optimizer": [{"lr": [4e-05, 4e-06, 4e-07], "optimizer": [{"__type__": "tests.sample.nested_subpackage.some_module.Adagrad"}]}, {"lr": [0.01, 0.1], "momentum": [0, 0.1], "optimizer": [{"__type__": "tests.sample.nested_subpackage.some_module.RMSprop"}]}]}'
    assert serialize_type_dict(type_serialization_test_dict) == out_string
