import hyperopt
from hyperopt.pyll.stochastic import sample
from hyperopt import STATUS_FAIL, STATUS_OK
from numpy.random import RandomState
from numpy import nan

from declair.search.grid import (possible_values)
from declair.search.hyperopt import (
                            hyperopt_params_to_params,
                            hyperopt_search_space)
from declair.search.search_info import (
                            insert_metrics_into_best_so_far,
                            is_metric_within_best_so_far)
from declair.exception import (AmbiguousSearchSpaceException,
                               InvalidConfigException)
from declair.search.hyperopt import opt_func
from declair.env import Environment

def test_possible_values_arbitrary_input():
    in_objects = [
        set([3,4,5]),
        'cats',
        3]
    for obj in in_objects:
        assert possible_values(obj) == [obj]

def test_possible_values_list():
    in_list = ['cats', 'dogs']
    assert possible_values(in_list) == in_list

def test_possible_values_simple_dict():
    in_dict = [{'function': 'Sigmoid'}]
    assert possible_values(in_dict) == in_dict

def test_possible_values_simple_dict_with_list():
    in_dict = [{'function': ['Sigmoid', 'ReLU']}]
    out_list = [{'function': 'Sigmoid'}, {'function': 'ReLU'}]
    assert possible_values(in_dict) == out_list

def test_possible_values_nested_lists():
    in_list = [[], [10], [10, 5]]
    try:
        possible_values(in_list)
        assert False
    except AmbiguousSearchSpaceException:
        assert True

    in_list = ['cat', {'__tuple__': [1,2,3]}]
    out_list = ['cat', (1, 2, 3)]
    assert possible_values(in_list) == out_list

    in_list = ['cat', {'__tuple__': [[0, 1],2,3]}]
    out_list = ['cat', (0, 2, 3), (1, 2, 3)]
    assert possible_values(in_list) == out_list

    in_list = ['cat', {'__tuple__': [
        {'meal': ['soup', 'bread']}, 'honey', [1,2]]}]
    out_list = ['cat',
                ({'meal': 'soup'}, 'honey', 1),
                ({'meal': 'soup'}, 'honey', 2),
                ({'meal': 'bread'}, 'honey', 1),
                ({'meal': 'bread'}, 'honey', 2)]
    assert possible_values(in_list) == out_list

def test_possible_values_list_of_dicts_with_only_lists():
    in_list = [
        {
            'function': 'Adagrad',
            'lr': [4e-05, 4e-06]
        },
        {
            'function': 'RMSprop',
            'lr': [1e-2, 1e-4],
            'momentum': [0, 1e-2]
        }
    ]
    out_list = [
        {'function': 'Adagrad', 'lr': 4e-05},
        {'function': 'Adagrad', 'lr': 4e-06},
        {'function': 'RMSprop', 'lr': 0.01, 'momentum': 0},
        {'function': 'RMSprop', 'lr': 0.01, 'momentum': 0.01},
        {'function': 'RMSprop', 'lr': 0.0001, 'momentum': 0},
        {'function': 'RMSprop', 'lr': 0.0001, 'momentum': 0.01}
    ]
    assert possible_values(in_list) == out_list

def test_possible_values_parameter_dict():
    in_search_dict = {    
        'model': ['model_sigmoid', 'model_relu'],
        'criterion': {
            'criterion': 'nllloss',
            'weights': {'__tuple__': [1, 2.4]}
        },
        'optimizer': [
            {
                'optimizer': 'Adagrad',
                'lr': [4e-05, 4e-06, 4e-07]
            },
            {
                'optimizer': 'RMSprop',
                'lr': [1e-2, 1e-1],
                'momentum': [0, 1e-1]
            }
        ],
    }
    out_search_list = [{'criterion': {'criterion': 'nllloss', 'weights': (1, 2.4)},
                        'model': 'model_sigmoid',
                        'optimizer': {'lr': 4e-05, 'optimizer': 'Adagrad'}},
                       {'criterion': {'criterion': 'nllloss', 'weights': (1, 2.4)},
                        'model': 'model_sigmoid',
                        'optimizer': {'lr': 4e-06, 'optimizer': 'Adagrad'}},
                       {'criterion': {'criterion': 'nllloss', 'weights': (1, 2.4)},
                        'model': 'model_sigmoid',
                        'optimizer': {'lr': 4e-07, 'optimizer': 'Adagrad'}},
                       {'criterion': {'criterion': 'nllloss', 'weights': (1, 2.4)},
                        'model': 'model_sigmoid',
                        'optimizer': {'lr': 0.01, 'momentum': 0, 'optimizer': 'RMSprop'}},
                       {'criterion': {'criterion': 'nllloss', 'weights': (1, 2.4)},
                        'model': 'model_sigmoid',
                        'optimizer': {'lr': 0.01, 'momentum': 0.1, 'optimizer': 'RMSprop'}},
                       {'criterion': {'criterion': 'nllloss', 'weights': (1, 2.4)},
                        'model': 'model_sigmoid',
                        'optimizer': {'lr': 0.1, 'momentum': 0, 'optimizer': 'RMSprop'}},
                       {'criterion': {'criterion': 'nllloss', 'weights': (1, 2.4)},
                        'model': 'model_sigmoid',
                        'optimizer': {'lr': 0.1, 'momentum': 0.1, 'optimizer': 'RMSprop'}},
                       {'criterion': {'criterion': 'nllloss', 'weights': (1, 2.4)},
                        'model': 'model_relu',
                        'optimizer': {'lr': 4e-05, 'optimizer': 'Adagrad'}},
                       {'criterion': {'criterion': 'nllloss', 'weights': (1, 2.4)},
                        'model': 'model_relu',
                        'optimizer': {'lr': 4e-06, 'optimizer': 'Adagrad'}},
                       {'criterion': {'criterion': 'nllloss', 'weights': (1, 2.4)},
                        'model': 'model_relu',
                        'optimizer': {'lr': 4e-07, 'optimizer': 'Adagrad'}},
                       {'criterion': {'criterion': 'nllloss', 'weights': (1, 2.4)},
                        'model': 'model_relu',
                        'optimizer': {'lr': 0.01, 'momentum': 0, 'optimizer': 'RMSprop'}},
                       {'criterion': {'criterion': 'nllloss', 'weights': (1, 2.4)},
                        'model': 'model_relu',
                        'optimizer': {'lr': 0.01, 'momentum': 0.1, 'optimizer': 'RMSprop'}},
                       {'criterion': {'criterion': 'nllloss', 'weights': (1, 2.4)},
                        'model': 'model_relu',
                        'optimizer': {'lr': 0.1, 'momentum': 0, 'optimizer': 'RMSprop'}},
                       {'criterion': {'criterion': 'nllloss', 'weights': (1, 2.4)},
                        'model': 'model_relu',
                        'optimizer': {'lr': 0.1, 'momentum': 0.1, 'optimizer': 'RMSprop'}}]
    for a, b in zip(possible_values(in_search_dict), out_search_list):
        assert a == b

def test_possible_values_complex_dict():
    in_dict = {
        'model': [
            'vgg16',
            {'name': 'feedforward',
             'shapes': [{'__tuple__': [40, 20]}, {'__tuple__': [40]}]},
        ],
        'optimizer': [
            {
                'optimizer': 'Adagrad',
                'lr': [4e-05, 4e-06, 4e-07]
            },
            {
                'optimizer': 'RMSprop',
                'lr': [1e-2, 1e-1],
                'momentum': [0, 1e-1]
            }
        ],
    }
    out_list = [{'model': 'vgg16', 'optimizer': {'lr': 4e-05, 'optimizer': 'Adagrad'}},
                {'model': 'vgg16', 'optimizer': {'lr': 4e-06, 'optimizer': 'Adagrad'}},
                {'model': 'vgg16', 'optimizer': {'lr': 4e-07, 'optimizer': 'Adagrad'}},
                {'model': 'vgg16',
                 'optimizer': {'lr': 0.01, 'momentum': 0, 'optimizer': 'RMSprop'}},
                {'model': 'vgg16',
                 'optimizer': {'lr': 0.01, 'momentum': 0.1, 'optimizer': 'RMSprop'}},
                {'model': 'vgg16',
                 'optimizer': {'lr': 0.1, 'momentum': 0, 'optimizer': 'RMSprop'}},
                {'model': 'vgg16',
                 'optimizer': {'lr': 0.1, 'momentum': 0.1, 'optimizer': 'RMSprop'}},
                {'model': {'name': 'feedforward', 'shapes': (40, 20)},
                 'optimizer': {'lr': 4e-05, 'optimizer': 'Adagrad'}},
                {'model': {'name': 'feedforward', 'shapes': (40, 20)},
                 'optimizer': {'lr': 4e-06, 'optimizer': 'Adagrad'}},
                {'model': {'name': 'feedforward', 'shapes': (40, 20)},
                 'optimizer': {'lr': 4e-07, 'optimizer': 'Adagrad'}},
                {'model': {'name': 'feedforward', 'shapes': (40, 20)},
                 'optimizer': {'lr': 0.01, 'momentum': 0, 'optimizer': 'RMSprop'}},
                {'model': {'name': 'feedforward', 'shapes': (40, 20)},
                 'optimizer': {'lr': 0.01, 'momentum': 0.1, 'optimizer': 'RMSprop'}},
                {'model': {'name': 'feedforward', 'shapes': (40, 20)},
                 'optimizer': {'lr': 0.1, 'momentum': 0, 'optimizer': 'RMSprop'}},
                {'model': {'name': 'feedforward', 'shapes': (40, 20)},
                 'optimizer': {'lr': 0.1, 'momentum': 0.1, 'optimizer': 'RMSprop'}},
                {'model': {'name': 'feedforward', 'shapes': (40,)},
                 'optimizer': {'lr': 4e-05, 'optimizer': 'Adagrad'}},
                {'model': {'name': 'feedforward', 'shapes': (40,)},
                 'optimizer': {'lr': 4e-06, 'optimizer': 'Adagrad'}},
                {'model': {'name': 'feedforward', 'shapes': (40,)},
                 'optimizer': {'lr': 4e-07, 'optimizer': 'Adagrad'}},
                {'model': {'name': 'feedforward', 'shapes': (40,)},
                 'optimizer': {'lr': 0.01, 'momentum': 0, 'optimizer': 'RMSprop'}},
                {'model': {'name': 'feedforward', 'shapes': (40,)},
                 'optimizer': {'lr': 0.01, 'momentum': 0.1, 'optimizer': 'RMSprop'}},
                {'model': {'name': 'feedforward', 'shapes': (40,)},
                 'optimizer': {'lr': 0.1, 'momentum': 0, 'optimizer': 'RMSprop'}},
                {'model': {'name': 'feedforward', 'shapes': (40,)},
                 'optimizer': {'lr': 0.1, 'momentum': 0.1, 'optimizer': 'RMSprop'}}]
    for a, b in zip(possible_values(in_dict), out_list):
        assert a == b

def test_possible_values_ignore_env():
    in_dict = {'__env__': ['some', 'multipart', 'other_key', 'impossible_key']}
    assert possible_values(in_dict) == [in_dict]

    in_dict = {'thingy': {"__env__": ["device"]}, 
               'number': [1,2,3]}
    assert possible_values(in_dict) == [
        {'number': 1, 'thingy': {"__env__": ["device"]}},
        {'number': 2, 'thingy': {"__env__": ["device"]}},
        {'number': 3, 'thingy': {"__env__": ["device"]}}
    ]

def test_possible_values_empty_list_exception():
    cases = [
        {'a': [1,2,3], 'b': []},
        [],
        {'soup': [1, 2, 3, {'cat': []}]}
    ]
    for case in cases:
        try:
            possible_values(case)
            assert False
        except InvalidConfigException:
            assert True

def test_hyperopt_search_space_type_assignment():
    assert isinstance(
        hyperopt_search_space({'__hp__': 'uniform', 'args': (1, 2)}),
        hyperopt.pyll.base.Apply)

def test_hyperopt_search_space_nested_lists_exception():
    in_list = [[], [10], [10, 5]]
    try:
        hyperopt_search_space(in_list)
        assert False
    except AmbiguousSearchSpaceException:
        assert True

def test_hyperopt_parameter_dtype_assignment():
    ints = [
        {'__hp__': 'quniform', 'args': (2, 8, 2)},
        {'__hp__': 'quniform', 'kwargs': {'low': 2, 'high': 8, 'q': 2}},
        {'__hp__': 'qloguniform', 'args': (4, 20, 4)},
        {'__hp__': 'uniform', 'args': (2, 8), 'dtype': 'int'},
        {'__hp__': 'randint', 'args': (10,)}]
    floats = [
        {'__hp__': 'uniform', 'args': (2, 8)},
        {'__hp__': 'quniform', 'args': (2, 8, 0.5)},
        {'__hp__': 'quniform', 'kwargs': {'low': 2, 'high': 8, 'q': 0.5}},
        {'__hp__': 'quniform', 'args': (2, 8, 2), 'dtype': 'float'}]
    processed_ints = [hyperopt_search_space(x) for x in ints]
    processed_floats = [hyperopt_search_space(x) for x in floats]
    for var in processed_ints:
        assert type(sample(var)) == int
    for var in processed_floats:
        assert type(sample(var)) == float

def test_hyperopt_categorical_possible_values():
    in_search_dict = {    
        'model': ['model_sigmoid', 'model_relu'],
        'criterion': {
            'criterion': 'nllloss',
            'weights': {'__tuple__': [1, 2.4]}
        },
        'optimizer': [
            {
                'optimizer': 'Adagrad',
                'lr': [4e-05, 4e-06, 4e-07]
            },
            {
                'optimizer': 'RMSprop',
                'lr': [1e-2, 1e-1],
                'momentum': [0, 1e-1]
            }
        ],
    }
    possible_vals = possible_values(in_search_dict)
    hyperopt_space = hyperopt_search_space(in_search_dict)
    for i in range(20):
        r = RandomState(i)
        assert sample(hyperopt_space, r) in possible_vals


def test_hyperopt_params_to_params():
    search_space_definition = {
        'model': ['vgg16',
                  {'name': 'perceptron',
                   'hidden_size': {'__hp__': 'quniform', 'args': (10, 50, 10)}
                   },
                  {'name': 'mlperceptron',
                   'hidden_sizes': {'__tuple__': [
                       {'__hp__': 'quniform', 'args': (10, 50, 10)}, 
                       {'__hp__': 'quniform', 'args': (10, 30, 10)}
                   ]}}],
        'optimizer':     [
            {
                'function': 'Adagrad',
                'lr': {'__hp__': 'lognormal', 'kwargs': {'mu': 1, 'sigma': 1}}
            },
            {
                'function': 'RMSprop',
                'lr': {'__hp__': 'uniform', 'args': (1e-4, 1e-2)},
                'momentum': 1e-2
            }
        ],
        'batch_size': [
            {'__hp__': 'quniform', 'args': (20, 60, 5)},
            {'__hp__': 'quniform', 'args': (400, 440, 5)}
        ]
    }
    hyperopt_params = {'root/batch_size': 0,
     'root/batch_size[0]': 60.0,
     'root/model': 1,
     'root/model[1]/hidden_size': 40.0,
     'root/optimizer': 0,
     'root/optimizer[0]/lr': 1.6102501640989249}
    actual_output = hyperopt_params_to_params(search_space_definition, hyperopt_params)
    correct_output = {
        'model': {
            'name': 'perceptron', 'hidden_size': 40
        },
        'optimizer': {
            'function': 'Adagrad',
            'lr': 1.6102501640989249,
        },
        'batch_size': 60
    }
    assert actual_output == correct_output

    hyperopt_params = {'root/batch_size': 0,
     'root/batch_size[0]': 420.0,
     'root/model': 2,
     'root/model[2]/hidden_sizes/0': 20.0,
     'root/model[2]/hidden_sizes/1': 20.0,
     'root/optimizer': 1,
     'root/optimizer[1]/lr': 0.0022019496801429354}

    actual_output = hyperopt_params_to_params(search_space_definition, hyperopt_params)
    correct_output = {
        'model': {
            'name': 'mlperceptron', 'hidden_sizes': (20, 20)
        },
        'optimizer': {
            'function': 'RMSprop',
            'lr': 0.0022019496801429354,
            'momentum': 1e-2
        },
        'batch_size': 420
    }
    assert actual_output == correct_output

def test_hyperopt_search_ignore_env():
    in_dict = {'__env__': ['some', 'multipart', 'other_key', 'impossible_key']}
    assert list(
        sample(hyperopt_search_space(in_dict))['__env__']) == in_dict['__env__']

def test_hyperopt_search_empty_list_exception():
    cases = [
        {'a': [1,2,3], 'b': []},
        [],
        {'soup': [1, 2, 3, {'cat': []}]}
    ]
    for case in cases:
        try:
            hyperopt_search_space(case)
            assert False
        except InvalidConfigException:
            assert True

def execute_faulty_run(params, _run):
        raise Exception('Run threw an exeception.')

def execute_run(params, _run):
    return {'opt_target': 1.0}

def test_hyperopt_search_returns_run_status_fail():
    env = Environment(dict={

    })

    config = {
        'type': 'search',
        'mode': 'hyperopt',
        'execute_function': {
            '__type__': 'tests.test_search.execute_faulty_run',
        },
        'experiment_name': 'test_status_fail',
        'search_params': {
            'fmin_kwargs':{
                'algo': {
                                    '__type__': 'hyperopt.tpe.suggest'
                },
                'max_evals': 1,   
            },
            'optimize': {
                'target': 'opt_target',
                'type': 'min'
            }
        },
        'params': {
            'test_variable': {
                '__hp__': 'quniform',
                'args': (0, 10, 1)
            }
        }
    }
    hyperopt_result = opt_func(run_params={
        'test_variable': 1
        }, 
        config=config, 
        env=env
    )
    assert 'status' in hyperopt_result and hyperopt_result['status'] == STATUS_FAIL

def test_hyperopt_search_returns_run_status_ok():
    env = Environment(dict={

    })

    config = {
        'type': 'search',
        'mode': 'hyperopt',
        'execute_function': {
            '__type__': 'tests.test_search.execute_run',
        },
        'experiment_name': 'test_status_fail',
        'search_params': {
            'fmin_kwargs':{
                'algo': {
                    '__type__': 'hyperopt.tpe.suggest'
                },
                'max_evals': 1,   
            },
            'optimize': {
                'target': 'opt_target',
                'type': 'min'
            }
        },
        'params': {
            'test_variable': {
                '__hp__': 'quniform',
                'args': (0, 10, 1)
            }
        }
    }
    hyperopt_result = opt_func(run_params={
        'test_variable': 1
        }, 
        config=config, 
        env=env
    )
    assert 'status' in hyperopt_result and \
        hyperopt_result['status'] == STATUS_OK and \
        'loss' in hyperopt_result and \
        hyperopt_result['loss'] == 1.0

def test_insert_metrics_into_best_so_far():
    test_metrics = [
        {'acc': 0.9, 'loss': 0.1},
        {'acc': 0.96, 'loss': 0.012},
        {'acc': 0.88, 'loss': 0.2},
        {'acc': 0.84, 'loss': 0.13},
        {'acc': 0.92, 'loss': 0.16},
        {'acc': nan, 'loss': nan}
    ]

    best_so_far = {}
    wanted_best_so_far = [
        {'bottom': {'acc': [0.9], 'loss': [0.1]}, 'top': {'acc': [0.9], 'loss': [0.1]}},
        {'bottom': {'acc': [0.9, 0.96], 'loss': [0.012, 0.1]}, 'top': {'acc': [0.96, 0.9], 'loss': [0.1, 0.012]}},
        {'bottom': {'acc': [0.88, 0.9, 0.96], 'loss': [0.012, 0.1, 0.2]}, 'top': {'acc': [0.96, 0.9, 0.88], 'loss': [0.2, 0.1, 0.012]}},
        {'bottom': {'acc': [0.84, 0.88, 0.9, 0.96], 'loss': [0.012, 0.1, 0.13, 0.2]}, 'top': {'acc': [0.96, 0.9, 0.88, 0.84], 'loss': [0.2, 0.13, 0.1, 0.012]}},
        {'bottom': {'acc': [0.84, 0.88, 0.9, 0.92, 0.96], 'loss': [0.012, 0.1, 0.13, 0.16, 0.2]}, 'top': {'acc': [0.96, 0.92, 0.9, 0.88, 0.84], 'loss': [0.2, 0.16, 0.13, 0.1, 0.012]}},
        {'bottom': {'acc': [0.84, 0.88, 0.9, 0.92, 0.96], 'loss': [0.012, 0.1, 0.13, 0.16, 0.2]}, 'top': {'acc': [0.96, 0.92, 0.9, 0.88, 0.84], 'loss': [0.2, 0.16, 0.13, 0.1, 0.012]}}
    ]
    for metrics, target in zip(test_metrics, wanted_best_so_far):
        insert_metrics_into_best_so_far(best_so_far, metrics, None)
        assert best_so_far == target

    test_metrics = [{'acc': nan, 'loss': nan}] + test_metrics

    best_so_far = {}
    wanted_best_so_far = [
        {'bottom': {}, 'top': {}},
        {'bottom': {'acc': [0.9], 'loss': [0.1]}, 'top': {'acc': [0.9], 'loss': [0.1]}},
        {'bottom': {'acc': [0.9, 0.96], 'loss': [0.012, 0.1]}, 'top': {'acc': [0.96, 0.9], 'loss': [0.1, 0.012]}},
        {'bottom': {'acc': [0.88, 0.9], 'loss': [0.012, 0.1]}, 'top': {'acc': [0.96, 0.9], 'loss': [0.2, 0.1]}},
        {'bottom': {'acc': [0.84, 0.88], 'loss': [0.012, 0.1]}, 'top': {'acc': [0.96, 0.9], 'loss': [0.2, 0.13]}},
        {'bottom': {'acc': [0.84, 0.88], 'loss': [0.012, 0.1]}, 'top': {'acc': [0.96, 0.92], 'loss': [0.2, 0.16]}},
        {'bottom': {'acc': [0.84, 0.88], 'loss': [0.012, 0.1]}, 'top': {'acc': [0.96, 0.92], 'loss': [0.2, 0.16]}}
    ]
    for metrics, target in zip(test_metrics, wanted_best_so_far):
        insert_metrics_into_best_so_far(best_so_far, metrics, 2)
        assert best_so_far == target

def test_is_metric_within_best_so_far():
    metric_name = 'acc'
    metric_value = 0.93
    top = True

    cases = [
        ({}, None, True),
        ({}, 3, True),
        ({'bottom': {'acc': [0.78, 0.85]}, 'top': {'acc': [0.85, 0.78]}}, 3, True),
        ({'bottom': {'acc': [0.78, 0.85]}, 'top': {'acc': [0.85, 0.78]}}, 2, True),
        ({'bottom': {'acc': [0.94, 0.95]}, 'top': {'acc': [0.95, 0.94]}}, 2, False),
        ({'bottom': {'acc': [0.92, 0.95]}, 'top': {'acc': [0.95, 0.92]}}, 2, True),
        ({'bottom': {'acc': [0.92, 0.95]}, 'top': {'acc': [0.95, 0.92]}}, 3, True),
        ({'bottom': {'acc': [0.02, 0.03]}, 'top': {'acc': [0.03, 0.02]}}, 2, True),
        ({'bottom': {'acc': [0.02, 0.03]}, 'top': {'acc': [0.03, 0.02]}}, 1, True),
        ({'bottom': {'acc': [0.92, 0.95]}, 'top': {'acc': [0.95, 0.92]}}, 1, False),
    ]
    for best_so_far, n, target in cases:
        assert is_metric_within_best_so_far(metric_name, metric_value, best_so_far, n, top) == target

    metric_name = 'loss'
    metric_value = 0.1
    top = False
    cases = [
        ({}, None, True),
        ({}, 3, True),
        ({'bottom': {'loss': [0.78, 0.85]}, 'top': {'loss': [0.85, 0.78]}}, 3, True),
        ({'bottom': {'loss': [0.78, 0.85]}, 'top': {'loss': [0.85, 0.78]}}, 2, True),
        ({'bottom': {'loss': [0.92, 0.95]}, 'top': {'loss': [0.95, 0.92]}}, 2, True),
        ({'bottom': {'loss': [0.92, 0.95]}, 'top': {'loss': [0.95, 0.92]}}, 3, True),
        ({'bottom': {'loss': [0.02, 0.03]}, 'top': {'loss': [0.03, 0.02]}}, 2, False),
        ({'bottom': {'loss': [0.02, 0.32]}, 'top': {'loss': [0.32, 0.02]}}, 2, True),
        ({'bottom': {'loss': [0.02, 0.32]}, 'top': {'loss': [0.32, 0.02]}}, 1, False)
    ]
    for best_so_far, n, target in cases:
        assert is_metric_within_best_so_far(metric_name, metric_value, best_so_far, n, top) == target

    metric_name = 'loss'
    metric_value = nan
    top = False
    cases = [
        ({}, None),
        ({}, 3),
        ({'bottom': {'loss': [0.78, 0.85]}, 'top': {'loss': [0.85, 0.78]}}, 3),
        ({'bottom': {'loss': [0.78, 0.85]}, 'top': {'loss': [0.85, 0.78]}}, 2),
        ({'bottom': {'loss': [0.92, 0.95]}, 'top': {'loss': [0.95, 0.92]}}, 2),
        ({'bottom': {'loss': [0.92, 0.95]}, 'top': {'loss': [0.95, 0.92]}}, 3),
        ({'bottom': {'loss': [0.02, 0.03]}, 'top': {'loss': [0.03, 0.02]}}, 2),
        ({'bottom': {'loss': [0.02, 0.32]}, 'top': {'loss': [0.32, 0.02]}}, 2),
        ({'bottom': {'loss': [0.02, 0.32]}, 'top': {'loss': [0.32, 0.02]}}, 1)
    ]
    for best_so_far, n in cases:
        assert is_metric_within_best_so_far(metric_name, metric_value, best_so_far, n, top) == False
