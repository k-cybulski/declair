from tempfile import NamedTemporaryFile

from declair.env import Environment
from declair.run.env import insert_env
from pytest import fixture
from declair.exception import EnvironmentException

@fixture
def sample_env():
    env = Environment()
    env.update_from_dict(
        {'singlepart_key': 'animals',
        'some': {
        'multipart': {
            'key': 'leaf',
            'other_key': 'zebra'
        }}}, 'test')
    return env

def test_env_dict_to_value(sample_env):
    in_dict = {'__env__': ['some', 'multipart', 'key']}
    assert insert_env(in_dict, sample_env) == 'leaf'

    in_dict = {'__env__': ['some', 'multipart', 'other_key', 'impossible_key']}
    try:
        print(insert_env(in_dict, sample_env))
        assert False
    except EnvironmentException:
        assert True

    in_dict = {'__env__': ['some', 'multipart', 'nonexistent_key'],
               'default': 'cat'}
    assert insert_env(in_dict, sample_env) == 'cat'

    in_dict = {'__env__': ['some', 'multipart', 'nonexistent_key']}
    try:
        print(insert_env(in_dict, sample_env))
        assert False
    except EnvironmentException:
        assert True

    in_dict = {'__env__': ['some', 'multipart', 'other_key', 'impossible_key'],
               'default': 'cat'}
    assert insert_env(in_dict, sample_env) == 'cat'

    in_dict = {'__env__': 'singlepart_key'}
    assert insert_env(in_dict, sample_env) == 'animals'

    in_dict = {'__env__': 'nonexistant_singlepart_key'}
    try:
        print(insert_env(in_dict, sample_env))
        assert False
    except EnvironmentException:
        assert True

def test_insert_env_into_list(sample_env):
    in_list = [1, 2, {'__env__': ['some', 'multipart', 'key']}]
    out_tuple = (1, 2, 'leaf')
    assert insert_env(in_list, sample_env) == out_tuple

def test_insert_env(sample_env):
    in_dict = {
        'cat': 'dog',
        'tree': {'__env__': ['some', 'multipart', 'key']}
    }
    out_dict = {
        'cat': 'dog',
        'tree': 'leaf'
    }
    assert insert_env(in_dict, sample_env) == out_dict

def test_env_dump(sample_env):
    # this isn't the usual usecase of NamedTemporaryFile. We won't be using the
    # file api directly, we just want a path we can use
    tfpath = NamedTemporaryFile().name
    sample_env.dump(tfpath)
    assert Environment.from_file(tfpath)._dict == sample_env._dict

def test_contains_nested_key(sample_env):
    test_cases = [
        (['some', 'multipart', 'key'], True),
        (['some', 'multipart', 'key', 'leaf'], False),
        (['cats'], False),
        (['some', 'multipart', 'other_key'], True)]
    for key, target in test_cases:
        assert sample_env.contains_nested_key(key) == target

def test_get_nested_key(sample_env):
    test_cases = [
        (['some', 'multipart', 'key'], 'leaf'),
        (['some', 'multipart', 'key', 'leaf'], None),
        (['cats'], None),
        (['some', 'multipart', 'other_key'], 'zebra')]
    for key, target in test_cases:
        if target is None:
            try:
                print(sample_env[key])
                assert False
            except KeyError:
                assert True
        else:
            assert sample_env.get_nested_key(key) == target

def test_set_nested_key(sample_env):
    test_key = ['some', 'multipart', 'key', 'leaf']
    test_value = 'dog'
    assert not sample_env.contains_nested_key(test_key)
    assert sample_env.get_nested_key(['some', 'multipart', 'key']) == 'leaf'

    sample_env.set_nested_key(test_key, test_value, 'test')
    assert sample_env.contains_nested_key(test_key)
    assert sample_env.get_nested_key(['some', 'multipart', 'key']) != 'leaf'
    assert sample_env.get_nested_key(['some', 'multipart', 'key', 'leaf']) == 'dog'
