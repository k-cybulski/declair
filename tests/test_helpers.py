from declair.helpers import manual

from .sample.functions import sample_manual_func

def test_manual():
    dict_ = {
        'manual': 'tests.sample.functions.sample_manual_func',
        'args': [1,2,3],
        'kwargs': {
            'a': 1,
            'b': 2,
            'c': 3
        }
    }
    assert manual(dict_) == ([1,2,3], {'a': 1, 'b': 2, 'c': 3})
    assert manual(dict_, 4,5,6) == ([1,2,3,4,5,6], {'a': 1, 'b': 2, 'c': 3})
    dict_ = {
        'manual': sample_manual_func,
        'args': [1,2,3],
        'kwargs': {
            'a': 1,
            'b': 2,
            'c': 3
        }
    }
    assert manual(dict_) == ([1,2,3], {'a': 1, 'b': 2, 'c': 3})
    assert manual(dict_, 4,5,6) == ([1,2,3,4,5,6], {'a': 1, 'b': 2, 'c': 3})
    assert manual(dict_, 4,5,6, default_kwargs={'d':4}) == ([1,2,3,4,5,6], {'a': 1, 'b': 2, 'c': 3, 'd':4})
    assert manual(dict_, 4,5,6, default_kwargs={'c':4}) == ([1,2,3,4,5,6], {'a': 1, 'b': 2, 'c': 3})

    dict_ = {
        'manual': sample_manual_func,
    }
    assert manual(dict_) == ([], {})
    assert manual(dict_, 1) == ([1], {})
    assert manual(dict_, default_kwargs={'cats': 4}) == ([], {'cats': 4})

    dict_ = {
        'manual': 'tests.sample.functions.sample_manual_func',
        'params': {
            'a': 1,
            'b': 2,
            'c': 3
        }
    }
    try:
        manual(dict_)
        assert False
    except ValueError:
        pass
