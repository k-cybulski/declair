import pytest
from tempfile import NamedTemporaryFile

from declair.serialization import load_file, dict_to_type
from declair.execution import execute_definition, execute_file
from declair.exception import InvalidExecuteFunctionException, RunException
from declair.env import Environment

from .sample.functions import _make_environment_clean, _is_environment_dirty

def load_from_string(string_):
    with NamedTemporaryFile() as file_:
        file_.write(string_.encode('utf-8'))
        file_.flush()
        config = load_file(file_.name)
    return config

@pytest.fixture
def empty_env():
    return Environment()

def test_valid_execute_function(empty_env):
    str_ = """
execute_function: {__type__: tests.sample.functions.sample_valid_execute_function}
experiment_name: test
type: run
params:
    x: 3
    y: 6
"""
    config = load_from_string(str_)
    # just check for crash
    execute_definition(config, empty_env)

def test_invalid_execute_function_list(empty_env):
    str_ = """
execute_function: {__type__: tests.sample.functions.sample_invalid_execute_function_list}
experiment_name: test
type: run
params:
    x: 3
    y: 6
"""
    config = load_from_string(str_)
    empty_env = Environment()
    try:
        execute_definition(config, empty_env)
        assert False
    except InvalidExecuteFunctionException:
        assert True

def test_run_exception(empty_env):
    config = load_from_string("""
execute_function: {__type__: tests.sample.functions.sample_possibly_crashing_function}
experiment_name: test
type: run
params:
    x: 5
    y: 6
""")
    try:
        execute_definition(config, empty_env)
        assert False
    except RunException:
        assert True

def test_cleanup_function_in_run(empty_env):
    config = load_from_string("""
execute_function: {__type__: tests.sample.functions.sample_execute_function_to_test_cleanup}
experiment_name: test
type: run
params:
    x: 5
    y: 6
""")
    _make_environment_clean() # just in case
    assert not _is_environment_dirty()
    
    execute_definition(config, empty_env)
    assert _is_environment_dirty()

    _make_environment_clean()
    assert not _is_environment_dirty() # as a test for whether the whole cleaning thing works

    config = load_from_string("""
execute_function: {__type__: tests.sample.functions.sample_execute_function_to_test_cleanup}
cleanup_function: {__type__: tests.sample.functions.sample_cleanup_function}
experiment_name: test
type: run
params:
    x: 5
    y: 6
""")

    execute_definition(config, empty_env)
    assert not _is_environment_dirty()

def test_cleanup_function_in_search_hyperopt(empty_env):
    config = load_from_string("""
type: search
mode: hyperopt
execute_function: {__type__: tests.sample.functions.sample_execute_function_to_test_cleanup}
experiment_name: test
search_params:
    fmin_kwargs:
        max_evals: 3
        algo: {__type__: hyperopt.tpe.suggest}
    optimize:
        type: max
        target: sum
params:
    x: {__hp__: loguniform, args: [-10, -2]}
    y: 3
""")
    # First ensure that the execute_function makes the environment dirty
    dict_to_type(config['execute_function'])(params={'x': 3,'y': 5})
    assert _is_environment_dirty()

    # First run a search without the cleanup function
    _make_environment_clean()
    execute_definition(config, empty_env)
    assert _is_environment_dirty()

    config = load_from_string("""
type: search
mode: hyperopt
execute_function: {__type__: tests.sample.functions.sample_execute_function_to_test_cleanup}
cleanup_function: {__type__: tests.sample.functions.sample_cleanup_function}
experiment_name: test
search_params:
    fmin_kwargs:
        max_evals: 3
        algo: {__type__: hyperopt.tpe.suggest}
    optimize:
        type: max
        target: sum
params:
    x: {__hp__: loguniform, args: [-10, -2]}
    y: 3
""")

    # Now run a search with the cleanup function
    _make_environment_clean()
    execute_definition(config, empty_env)
    assert not _is_environment_dirty()

def test_cleanup_function_in_search_grid(empty_env):
    config = load_from_string("""
type: search
mode: grid
execute_function: {__type__: tests.sample.functions.sample_execute_function_to_test_cleanup}
experiment_name: test
search_params:
    runs_per_configuration: 1
params:
    x: [0.01, 0.1]
    y: 3
""")
    # First ensure that the execute_function makes the environment dirty
    dict_to_type(config['execute_function'])(params={'x': 3,'y': 5})
    assert _is_environment_dirty()

    # First run a search without the cleanup function
    _make_environment_clean()
    execute_definition(config, empty_env)
    assert _is_environment_dirty()

    config = load_from_string("""
type: search
mode: grid
execute_function: {__type__: tests.sample.functions.sample_execute_function_to_test_cleanup}
cleanup_function: {__type__: tests.sample.functions.sample_cleanup_function}
experiment_name: test
search_params:
    runs_per_configuration: 1
params:
    x: [0.01, 0.1]
    y: 3
""")
    # Now run a search with the cleanup function
    _make_environment_clean()
    execute_definition(config, empty_env)
    assert not _is_environment_dirty()

def test_execute_file_run():
    execute_file('tests/sample/extend.yaml')

def test_execute_file_extend():
    execute_file('tests/sample/extend.yaml')

def test_execute_reproduce_experiment():
    assert execute_file('tests/sample/old_experiment_with_env.json') == {
        'sum': 0, 'just_x': 3}
