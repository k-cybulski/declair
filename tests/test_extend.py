import yaml

from declair.extend.insertion import _recursive_extend_from

def test_recursive_extend():
    cases = [
        ("""
simple: 3
parameters:
    model:
        type: perceptron
        layers: 3
        outputs: 2
        final_activation:
            type: relu
            mode: leaky
    optimizer:
        type: adam
        lr: 3
        """,
        """
parameters:
    model:
        type: cake
        __extend__: replace
    optimizer:
        lr: 1
        """,
        """
simple: 3
parameters:
    model:
        type: cake
    optimizer:
        type: adam
        lr: 1
        """),
        ("""
simple: 3
parameters:
    model:
        type: complex_multilayer_thing
        layers: [2, 5, 3]
    optimizer:
        type: adam
        lr: 3
        """,
        """
parameters:
    optimizer:
        lr: 1
        """,
        """
simple: 3
parameters:
    model:
        type: complex_multilayer_thing
        layers: [2, 5, 3]
    optimizer:
        type: adam
        lr: 1
        """),
        ("""
simple: 3
parameters:
    model:
        type: complex_multilayer_thing
        layers: [2, 5, 3]
    optimizer:
        type: adam
        lr: 3
        """,
        """
{}
        """,
        """
simple: 3
parameters:
    model:
        type: complex_multilayer_thing
        layers: [2, 5, 3]
    optimizer:
        type: adam
        lr: 3
        """),
        ("""
simple: 3
parameters:
    model:
        type: complex_multilayer_thing
        layers: [2, 5, 3]
    optimizer:
        type: adam
        lr: 3
        """,
        """
simple: {__extend__: delete}
parameters:
    __extend__: replace
        """,
        """
parameters:
    {}
        """),
        ("""
simple: 3
parameters:
    model:
        type: complex_multilayer_thing
        layers: [2, 5, 3]
    optimizer:
        type: adam
        lr: 3
        """,
        """
simple:
    actually_not_this_is_complex:
        param1: 3
        param2: cats
parameters:
    model:
        type: complex_multilayer_thing
        layers: {__extend__: delete}
        """,
        """
simple:
    actually_not_this_is_complex:
        param1: 3
        param2: cats
parameters:
    model:
        type: complex_multilayer_thing
    optimizer:
        type: adam
        lr: 3
        """),
        ("""
simple: 3
parameters:
    model:
        type: complex_multilayer_thing
        layers: [2, 5, 3]
    optimizer:
        type: adam
        lr: 3
        """,
        """
simple:
    actually_not_this_is_complex:
        __extend__: replace
        param1: 3
        param2: soup
        param3:
            this_should_be_removed_even_though_it_doesnt_exist_in_the_parent: 
                __extend__: delete
            elephant: long_trunk
            something_to_replace:
                __extend__: replace
                sky: blue
parameters:
    __extend__: delete
    model:
        type: complex_multilayer_thing
        layers: {__extend__: delete}
        """,
        """
simple:
    actually_not_this_is_complex:
        param1: 3
        param2: soup
        param3:
            elephant: long_trunk
            something_to_replace:
                sky: blue
        """)
    ]
    for parent_str, blueprint_str, target_str in cases:
        parent = yaml.safe_load(parent_str)
        blueprint = yaml.safe_load(blueprint_str)
        target = yaml.safe_load(target_str)
        assert _recursive_extend_from(blueprint, parent) == target
