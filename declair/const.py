DEF_KEY_EXECUTE_TIME = 'execute_time'
DEF_KEY_TYPE = 'type'
DEF_KEY_ENV_PARAMS = 'env_params'
DEF_KEY_SEARCH_MODE = 'mode'
DEF_KEY_SEARCH_INFO = 'search_info'
DEF_KEY_EXECUTE_FUNCTION = 'execute_function'
DEF_KEY_CLEANUP_FUNCTION = 'cleanup_function'
DEF_KEY_EXPERIMENT_NAME = 'experiment_name'
DEF_KEY_EXTEND_PARENT = 'extend_from'
DEF_KEY_EXTEND_CHAIN = 'extend_chain'
DEF_KEY_VARIABLE_DICT = 'variables'
DEF_KEY_PARAMS = 'params'
DEF_KEY_STATIC_PARAMS = 'static_params'
DEF_KEY_SEARCH_PARAMS = 'search_params'

DEF_TYPE_RUN = 'run'
DEF_TYPE_SEARCH = 'search'
DEF_TYPE_EXTEND = 'extend'

DEF_STORED_RUN_CONFIG_NAME = 'declair_format_config.json'
DEF_STORED_SEARCH_CONFIG_NAME = 'declair_format_search_config.json'
