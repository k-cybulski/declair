"""
Keys of special extend definition entries.
"""

EXTEND_KEY = '__extend__'
EXTEND_VALUE_DELETE = 'delete'
EXTEND_VALUE_REPLACE = 'replace'
