"""
This subpackage contains functions for dealing with run experiment definition
dictionaries. This mainly means inserting correct values into special entries
like {'__type__': 'some.type'}
"""
