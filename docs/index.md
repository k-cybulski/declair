# Declair
[![pipeline status](https://gitlab.com/k-cybulski/declair/badges/master/pipeline.svg)](https://gitlab.com/k-cybulski/declair/-/commits/master)
[![coverage report](https://gitlab.com/k-cybulski/declair/badges/master/coverage.svg)](https://gitlab.com/k-cybulski/declair/-/commits/master)

[Declair](https://gitlab.com/k-cybulski/declair) is a framework for declaratively defining hyperparameter optimization experiments. It uses [Sacred](https://github.com/IDSIA/sacred) for storing experiment results and supports [Hyperopt](https://github.com/hyperopt/hyperopt) for optimization.

At its core, Declair provides a YAML/JSON based language for defining parameter spaces for hyperparameter search as well as single experiment runs. These parameter spaces can be easily nested and may include Python objects and outputs of function calls.

Declair is focused on reproducibility and ease of use between machines. By the use of [Sacred observers](https://sacred.readthedocs.io/en/stable/observers.html), it ensures that results of experiments are safely stored, together with the source code of all classes and objects used in them, as well as the Declair experiment configuration itself. To make reproduction of experiments easy between machines, it supports environment configuration files which can be used to store variables, like local dataset paths or secret tokens.

It contains various features to make defining experiments ergonomic. These include variable handling as well as configuration inheritance.

## Installation
To install Declair, simply use `pip` in your favourite virtual environment.
```
pip install declair
```

## Usage
To see how you can use Declair yourself, go to the [Usage](usage.md) section. For an example Declair project, see below.

## Basic example
Below we present how to set up and run a simple experiment with Declair and sklearn. In this example, we will try some models for a classification task on two datasets.

In particular, we will do a hyperparameter search over:

- Two configurations of SVMs, with varying kernels.
- Multiple random forest classifiers, with a varying number of estimators.

For each model, we will compute its cross validated score and plot a confusion matrix plot to see which classes are easy to confuse.

First, we will define the experiment procedure which takes in a `model` and `dataset` from the experiment configuration, and then computes the cross validation score and creates the plot.

Make a stub Python package `research_project` (so, create a directory with an empty file `research_project/__init__.py`), and let the file below be a module in the package `research_project/execute.py`:
```python
import numpy as np
from matplotlib import pyplot as plt
from sklearn.model_selection import cross_val_score, train_test_split
from sklearn.metrics import plot_confusion_matrix

from declair.results import add_artifact_fig

def execute_experiment(params, _run):
    model = params['model'] # We assume this to be an sklearn estimator
    dataset = params['dataset'] # We assume this to be an sklearn dataset

    x = dataset['data']
    y = dataset['target']

    # First, to examine if this model performs well, we will compute its cross validated score.
    cv_score = np.mean(cross_val_score(model, x, y, cv=5))

    # Then, we would like to see which classes are being confused with which.
    # For that purpose, we will make a confusion matrix plot
    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.33, random_state=42
    )
    model.fit(x_train, y_train)

    # Keep a test score for reference
    test_score = model.score(x_test, y_test)

    # Create a figure and plot the matrix
    fig, ax = plt.subplots()
    plot_confusion_matrix(model, x_test, y_test, ax=ax, xticks_rotation='vertical')
    fig.set_size_inches(10, 10)

    # Save the figure as an artifact
    add_artifact_fig(_run, fig, "confusion_matrix.png")

    return {
        'cv_score': cv_score,
        'test_score': test_score
    }
```

Now, we can define our search space like this in a file, say, `search_digits.yaml` (since we're using the digits sklearn dataset):
```yaml
type: "search"
mode: "hyperopt"
# The execution function we defined above
execute_function: {"__type__": "research_project.execute.execute_experiment"}

# Configuration of Hyperopt for hyperparameter search
search_params:
    fmin_kwargs:
        max_evals: 10
        algo: {__type__: hyperopt.tpe.suggest}
    optimize:
        type: "max"
        target: "cv_score"

# params argument of the execute function will be sampled from the parameter
# search space defined below
params:
    # Every list in a search space definition defines a disjoint choice. Below,
    # the model is chosen from two options with multiple smaller sub-choices
    model:
        # __call__ entries are dynamically loaded and called before being
        # passed on to the execute function
        - __call__: "sklearn.svm.SVC"
          kwargs:
              kernel: ['rbf', 'linear']
        - __call__: "sklearn.ensemble.RandomForestClassifier"
          kwargs:
              # n_estimators below is sampled from a numeric distribution with Hyperopt
              n_estimators:
                  __hp__: quniform
                  args: [1, 10, 1]
    dataset:
        __call__: "sklearn.datasets.load_digits"
```

Now, for the final bit, we need to define where results of runs will be stored. [This can be local file storage or MongoDB.](https://k-cybulski.gitlab.io/declair/usage/#configuring-where-to-store-results) For this example, we will simply store them in a directory called `out`. So, create a file called `declair_env.yaml` (this is the default Declair environment config name):
```yaml
observers:
    file:
        path: 'out'
```

Now, the file tree should look like this:
```
.
├── declair_env.yaml
├── research_project
│   ├── execute.py
│   └── __init__.py
└── search_digits.yaml
```

To run the search experiment, run:
```
declair-execute search_digits.yaml
```
And poof, the experiments should now be running with all results stored in the `out` directory. In the directory of every run, other than the results and plots, there will also be a run definition file `declair_format_config.json`, which can also be executed with `declair-execute` to reproduce an experiment with these parameters.

We could now run the same search experiment as above, but with a second dataset. To do that, we can inherit the configuration from `search_digits.yaml` while only changing the dataset. Create a file `search_iris.yaml`:
```yaml
type: "extend"
extend_from: "search_digits.yaml"

params:
    dataset:
        __call__: "sklearn.datasets.load_iris"
```

To run the search experiment on the iris dataset, now simply run:
```
declair-execute search_digits.yaml
```


## Example project
The [Declair repository](https://gitlab.com/k-cybulski/declair) contains an example project with Declair experiment definitions in the `examples` directory:

- `examples/sample_project` contains Python code of the dummy project.
- `examples/sample_configs` contains Declair experiment definitions and environment configs.

To run these examples, make sure `declair` is pip-installed, as opposed to just being in the working directory; if it is installed correctly, there will be a command `declair-execute` available. If is not available, then you need to `pip` install it like below (assuming you're in the root repo directory).
```bash
pip install -e .
```
The example project also uses PyTorch and a few dependencies, so you will need to
```bash
pip install -r examples/requirements-torch.txt
```

Now, to run examples, you can just use the `declair-execute` command like:
```bash
declair-execute <example-definition>
```
where `<example definition>` is an experiment definition from the `examples/sample_configs` directory. 

Furthermore, in that directory there are two environment configurations:

- `declair_env.yaml` (default) which just saves outputs to an `out` directory.
- `declair_env_with_mongodb.yaml` which also saves the outputs to a MongoDB database on `localhost`, port `27017` with a user `mongo_user` and password `mongo_password`; this is the default configuration from a [Docker example for Sacred](https://sacred.readthedocs.io/en/stable/examples.html#docker-setup).

To use the MongoDB configuration, run `declair-execute` with the `-e` argument like:
```bash
declair-execute <example definition> -e examples/sample_configs/declair_env_with_mongodb.yaml
```
