# What's this?
Declair came about from attempts to recreate [DeepSolaris](https://gitlab.com/CBDS/DeepSolaris) results in PyTorch instead of Keras, with a focus on search experiment definition ergonomics and reproducibility. However, it grew to be a more extensive and general framework than originally planned. Its design based on configuration files was heavily inspired by [cbds_common](https://gitlab.com/CBDS/cbds_common).

From [CBDS](https://www.cbs.nl/en-gb/our-services/unique-collaboration-for-big-data-research) with ❤️
