# Usage
Declair is based around YAML/JSON _experiment definitions_ which are parsed and used to provide arguments to an _execute function_. There are two distinct types of experiments:

- Run experiments, in which the execute function is executed once with parameters defined in the experiment definition.
- Search experiments, in which the experiment definition is parsed as a search space of possible parameters, and from which multiple run experiments are constructed and executed.

In both cases, results of runs are stored by the use of Sacred observers in locations defined in an environment YAML configuration file.

The experiment definition config files can contain special entries which are parsed before the parameters are provided to the execute function. This allows, for example, for passing dynamically imported types and initialized objects in the parameter dictionary. 

An experiment definition can be simply executed by running the `declair-execute` command:
```bash
declair-execute <experiment-config>
```

## Run experiments
To define a run experiment, it is necessary to wrap the experiment code in the execute function which accepts a `params` argument for the parameter dictionary and a `_run` argument , which will be a [Sacred Run](https://sacred.readthedocs.io/en/stable/apidoc.html#the-run-object) used for storing results. The execute function is responsible for tracking results in Sacred; see the [Sacred Run object docs](https://sacred.readthedocs.io/en/stable/apidoc.html#the-run-object) to see how this is done. Methods `add_artifact` and `log_scalar` are particularly of interest, as they allow for storing artifact files and for keeping track of metrics over the course of the run. Declair comes with some helper functions for tracking results, but only for PyTorch Ignite so far.

A run definition YAML looks like this:
```yaml
type: "run"
execute_function: {"__type__": "some.module.function"}
experiment_name: "name for reference in Sacred"
params:
    some_arg: "some value"
    some_function_arg: {"__type__": "some.module.another_function"}
    [...]
```

Upon execution, the function `some.module.function` is executed with `params` as its `params` keyword argument. The results of the experiment are stored in Sacred with `experiment_name` as the experiment name. If `experiment_name` is not given, it is set to the name of the file. Location of storage is defined in the environment configuration, described further down.

For an example run, see `examples/sample_configs/run.json` with the associated
execution function in `examples/sample_project/execute.py`

### Special entries
Run experiment definitions can contain special entries which are parsed before being passed as parameters, like those of form `{"__type__": [...]}` in the example above. All special entries used in run definitions are described below, in order of how they are parsed in the code:
 
#### Type `__type__`

Type entries are dynamically imported Python types. An entry of form `{"__type__": "some.module.function"}` is loaded into memory as if it was imported with `import some.module.function`.

#### Environment `__env__`

Environment entries are used to read variables from the [environment configuration](#environment-configuration), which is a nested dictionary. These entries are of the form
```yaml
{
    "__env__": <key>
}
```
where `<key>` can either be a list of strings or a string. If a list of strings, say `[long, nested, key]`, it is equivalent to `env['long']['nested']['key']`. Furthermore, the `__env__` YAML/JSON object can also contain a `default` entry which is the value returned if the key is not found in the environment dictionary:
```yaml
{
    "__env__": <key>,
    "default": some_value
}
```

#### Variable `__var__`

Variable entries are inserted from a dictionary of given in the experiment definition. Usually, these are used in search described further down, where a single value can be sampled and used in multiple places in the parameters. If a `{"__var__": variable_name}` entry is used, it is necessary to include a `variables` dictionary inside the experiment definition like:
```yaml
type: "run"
execute_function: [...]
experiment_name: [...]
params:
    [...]
variables:
    variable_name: 1
    variable_another_name: 3
    [...]
```

#### Call `__call__`

Call entries are used to insert outputs of Python function calls into the parameter dictionary. A `__call__` entry has two optional keys `args` and `kwargs` which defines arguments and keyword arguments passed to the function:
```yaml
{
    "__call__": function.to.call,
    "args": [args, to, function],
    "kwargs": {
        "kwarg_one": 2,
        "kwarg_two": 5
    }
}
```

#### Variable instance `__var_instance__`

Variable instance entries work the same as `__var__` entries, but they are inserted only after `__call__` entries have been parsed. This means that a `__var_instance__` can be an instance of an object obtained by calling a function, which can then be used in multiple places in the parameter dictionary.

## Search experiments
There are two different modes of search experiments:

- `grid` experiments, which create runs from a simple gridsearch.
- `hyperopt` experiments, which sample runs from a Hyperopt search space.

Basic structure of grid and hyperopt search experiments is similar to run
experiments, with an additional trick: in the `params` entry, any list defines
multiple disjoint possible configurations, with each item of the list defining
one possible configuration.

As a very simple example, if a dictionary like
```python
{'optimizer': [
    {
        'function': 'Adagrad',
        'lr': [4e-05, 4e-06]
    },
    {
        'function': 'RMSprop',
        'lr': [1e-2, 1e-4],
        'momentum': [0, 1e-2]
    }
]}
```
finds its way into the search definition, it will be _unrolled_ into such
possible values:
```python
[{'optimizer': {'function': 'Adagrad', 'lr': 4e-05}},
 {'optimizer': {'function': 'Adagrad', 'lr': 4e-06}},
 {'optimizer': {'function': 'RMSprop', 'lr': 0.01, 'momentum': 0}},
 {'optimizer': {'function': 'RMSprop', 'lr': 0.01, 'momentum': 0.01}},
 {'optimizer': {'function': 'RMSprop', 'lr': 0.0001, 'momentum': 0}},
 {'optimizer': {'function': 'RMSprop', 'lr': 0.0001, 'momentum': 0.01}}]
```
This works for any arbitrary level of nestedness of dictionaries and lists.
However, note that lists nested directly inside lists are ambiguous. So, in
case you wish to use an actual sequence for a parameter, you can do so by
wrapping the list `[...]` with `{"__tuple__": [...]}`. Entries of the sequence
will also support unrolling. For example, a list of options
```python
{'__tuple__': ['cat', {'__tuple__': [[0, 1],2,3]}]}
```
is unrolled into
```python
[('cat', (0, 2, 3)), 
 ('cat', (1, 2, 3))]
```
Furthermore, Hyperopt search space definitions also support special entries of
form `{"__hp__": "<hyperopt-param>", "args": (...)}` or `{"__hp__": "<hyperopt-param>", "kwargs": {...}}` where `<hyperopt-param>` is one of
hyperopt parameter expressions as in
[here](https://github.com/hyperopt/hyperopt/wiki/FMin#21-parameter-expressions)
(but without `hp.`) with the corresponding arguments or keyword arguments. The
data type (`float` or `int`) of the parameter will be inferred, however if you
need to be sure you can add `"dtype": "int"` or `"dtype": "float"` to the
entry.

So, with all that in mind, the basic structure of a search experiment is:
```yaml
type: "search"
mode: <"grid" or "hyperopt">
execute_function: {"__type__": "some.module.function"}
experiment_name: "name for reference in Sacred"
search_params:
   [...]
params:
   "some_arg": ["some value", "some other value"],
   "some_function_arg": {"__type__": "some.module.another_function"},
   [...]
```
Here the new element compared to a run definition is `search_params`, which contains settings of the search method.

### Grid search params

For grid search experiments, `search_params` are:
```python
"search_params": {
    "runs_per_configuration": <number of times each configuration is executed>
}
```

For a sample gridsearch, see `examples/sample_configs/search_grid.json`.

### Hyperopt search params

For Hyperopt search experiments, `search_params` are:
```python
"search_params": {
    "fmin_kwargs": {
        <hyperopt.fmin keyword arguments>
    },
    "optimize": {
        "type": <"max" or "min">,
        "target": "target variable"
    }
}
```
`fmin_kwargs` defines keyword arguments to `hyperopt.fmin` (see [here](https://github.com/hyperopt/hyperopt/wiki/FMin))

`optimize` defines which variable we wish to optimize. `target` defines which
key of the execute function output dictionary to optimize; if the execute
function outputs a scalar, do not provide this setting. `type` defines whether
it's a `max` or `min` optimization problem.

For a sample Hyperopt search, see `examples/sample_configs/search_hyperopt.json`.

## Environment configuration
It is possible to insert more information into runs from the environment, such
as data directory paths. Environment configuration is stored in YAML files.

For an experiment definition `path/definition.json`, the environment
configuration files are loaded in this order (each lower file can overwrite
settings of the files before it):

 - `declair_env.yaml` in the root of the git repository of the file
   `path/definition.json`, if it's in a git repository.
 - `path/declair_env.yaml`
 - `path/definition.json.declair_env.yaml`

To insert an entry into the experiment definition, write 
`{"__env__": [... key ...]}` where the key is a list of nested entries to
follow.

For example, if you have a YAML
```yaml
dataset:
    path: "/storage/data/sample_dataset"
```
and wish to use `path` for an experiment, in the experiment definition write
`{"__env__": ["dataset", "path"]}` in place where you wish to use it.

### Configuring where to store results
The environment config YAML files are used to define where to store experiment
results. [Sacred
observers](https://sacred.readthedocs.io/en/stable/observers.html) are used to
store results. As of now, only Mongo Observer and File Storage Observer are
working.

Observers are defined in the YAML as follows:
```yaml
observers:
    file:
        path: "out/test_run"
    mongo:
        url: "mongodb://mongo_user:mongo_password@localhost:27017"
```
Here `file` and `path` define the directory to store File Storage Observer
outputs. `mongo` and `url` define the url to a database for the Mongo Observer.
If either is not provided, that observer is not used. If an empty config is
given, no observers are used and thus no results are stored.

## Experiment inheritance
It is possible to extend experiment definitions and only change some specific entries. This is done by the experiment mode `extend` with a key `extend_from` which defines the relative path from the extending config to the parent config. For example, if we have an experiment definition `experiment_run.yaml` with the following contents
```yaml
type: "run"
execute_function: {"__type__": "some.module.function"}
experiment_name: "An experiment"
params:
    some_arg: "some value"
    some_function_arg: {"__type__": "some.module.another_function"}
    a_dictionary:
        cats: 1
        dogs: 5
```
and we write a child definition
```yaml
type: "extend"
extend_from: "experiment_run.yaml"
experiment_name: "A cooler experiment"
params:
    a_dictionary:
        dogs: "A string"
```
then if we execute the above child definition, Declair will parse it as
```yaml
type: "run"
execute_function: {"__type__": "some.module.function"}
experiment_name: "A cooler experiment"
params:
    some_arg: "some value"
    some_function_arg: {"__type__": "some.module.another_function"}
    a_dictionary:
        cats: 1
        dogs: "A string"
```

While extending, only the specified nested keys of the dictionaries will be modified; other dictionary entries remain the same. If a non-dictionary entry like a list or an object is modified, it is changed fully to the content of the child. 

Furthermore, the `extend` mode includes a few special commands denoted by `__extend__` entries.

### Delete command `__extend__: delete`

The `{__extend__: delete}` command will delete a key from the resulting dictionary. So, with the same parent `experiment_run.yaml` as above,
```yaml
type: "extend"
extend_from: "experiment_run.yaml"
experiment_name: "A cooler experiment"
params:
    a_dictionary:
        dogs: {__extend__: delete}
```
will result in
```yaml
type: "run"
execute_function: {"__type__": "some.module.function"}
experiment_name: "A cooler experiment"
params:
    some_arg: "some value"
    some_function_arg: {"__type__": "some.module.another_function"}
    a_dictionary:
        cats: 1
```

### Replace command `__extend__: replace`

The `{__extend__: replace}` command will fully replace a parent dictionary with entries from the child. So, continuing with the `experiment_run.yaml` example from before,
```yaml
type: "extend"
extend_from: "experiment_run.yaml"
experiment_name: "A cooler experiment"
params:
    {__extend__: replace}
    soup: "Beans"
```
will result in
```yaml
type: "run"
execute_function: {"__type__": "some.module.function"}
experiment_name: "A cooler experiment"
params:
    soup: "Beans"
```
